package com.interview.glovo;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

@Service
@Slf4j
public class TaskService {

    public boolean isSubArraySumEqualsToZero(int[] array) {

        if (array.length == 0) {
            return false;
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = i; j < array.length; j++) {

                int value = array[j];

                if (value == 0) {
                    return true;
                }

                int subArraySum = sumArrayValues(array, i, j);

                if (subArraySum == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private int sumArrayValues(int[] array, int left, int right) {
        int sum = 0;
        for (int i = left; i <= right; i++) {
            sum += array[i];
        }
        return sum;
    }

    public int[] alternatingSorting(int[] array) {

        int[] sortedArray = new int[array.length];

        if (array.length < 2) {
            return array;
        }
        if (array.length == 2) {
            int first = array[0];
            int second = array[1];
            array[0] = Math.min(first, second);
            array[1] = Math.max(first, second);
        }

        Arrays.sort(array);

        boolean takeFirst = true;

        for (int idx = 0; idx < array.length; idx++) {

            if (takeFirst) {
                sortedArray[idx] = array[idx];
                takeFirst = false;
            } else {
                sortedArray[idx] = array[array.length - 1 - idx];
                takeFirst = true;
            }
        }
        return sortedArray;
    }

    public String getFirstRepeatedCharacter(String string) {

        if (Strings.isEmpty(string)) {
            return null;
        }

        HashMap<Character, Integer> characters = new LinkedHashMap<>();

        for (int idx = 0; idx < string.length(); idx++) {
            Character character = string.charAt(idx);

            characters.put(character, characters.getOrDefault(character, 0) + 1);
        }

        for (Character character : characters.keySet()) {
            if (characters.get(character) >= 2) {
                return character.toString();
            }
        }
        return null;
    }
}
