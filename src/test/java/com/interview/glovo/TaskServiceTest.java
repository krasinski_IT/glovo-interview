package com.interview.glovo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class TaskServiceTest {

    @Autowired
    TaskService taskService;

    @Test
    void shouldReturnTrueWhenThereIsSubarrayWithSumZero() {

        int[] inputArray = new int[]{4, 2, -3, 1, 6};
        assertTrue(taskService.isSubArraySumEqualsToZero(inputArray));

        int[] secondInputArray = new int[]{4, 2, 0, 1, 6};
        assertTrue(taskService.isSubArraySumEqualsToZero(secondInputArray));

        int[] thirdInputArray = new int[]{-3, 2, 3, 1, 6};
        assertFalse(taskService.isSubArraySumEqualsToZero(thirdInputArray));
    }

    @Test
    void shouldDoAlternateSorting() {
        int[] inputArray = new int[]{5, 2, 1, 7, 9, 8};

        int[] sortedArray = taskService.alternatingSorting(inputArray);
        log.info(Arrays.toString(sortedArray));

        for (int i = 1; i < sortedArray.length; i++) {

            if (i % 2 == 1) {
                assertTrue(sortedArray[i] >= sortedArray[i - 1]);

            } else {
                assertTrue(sortedArray[i] <= sortedArray[i - 1]);
            }
        }
    }

    @Test
    void shouldReturnFirstRepeatedCharacter() {

        String firstTestResult = taskService.getFirstRepeatedCharacter("ABCA");
        assertEquals("A", firstTestResult);

        String secondTestResult = taskService.getFirstRepeatedCharacter("BCABA");
        assertEquals("B", secondTestResult);

        String thirdTestResult = taskService.getFirstRepeatedCharacter("glovoll");
        assertEquals("l", thirdTestResult);

        String fourthTestResult = taskService.getFirstRepeatedCharacter("ABC");
        assertNull(fourthTestResult);

        String fifthTestResult = taskService.getFirstRepeatedCharacter("");
        assertNull(fifthTestResult);

        String sixthTestResult = taskService.getFirstRepeatedCharacter(null);
        assertNull(sixthTestResult);
    }
}
